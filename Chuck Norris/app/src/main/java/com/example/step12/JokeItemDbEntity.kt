package com.example.step12

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName ="Current_jokes",
)

data class JokeItemDbEntity(
    @PrimaryKey val id:Int,
    val body:String
) {

    fun toJokeItem() = JokeItem(
        id = id,
        body = body
    )

}