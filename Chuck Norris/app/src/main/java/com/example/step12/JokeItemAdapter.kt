package com.example.step12

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.step12.databinding.JokeItemBinding

class JokeItemAdapter():RecyclerView.Adapter<JokeItemAdapter.JokeItemHolder>() {

    val myJokeItemList = ArrayList<JokeItem>()

    class JokeItemHolder(item: View):RecyclerView.ViewHolder(item) {
        val binding = JokeItemBinding.bind(item)
        fun bind(postItem: JokeItem){
            binding.tvText.text = postItem.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokeItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.joke_item,parent,false)
        return JokeItemHolder(view)
    }

    override fun onBindViewHolder(holder:JokeItemHolder, position: Int) {
        holder.bind(myJokeItemList[position])
    }

    override fun getItemCount(): Int = myJokeItemList.size

    fun addPostItem(item: JokeItem) {
        myJokeItemList.add(item)
        notifyDataSetChanged()
    }

//    fun addAllPostItem(List<JokeItemDbEntity>){
//
//    }

    fun clearAll(){
        myJokeItemList.clear()
        notifyDataSetChanged()
    }
}