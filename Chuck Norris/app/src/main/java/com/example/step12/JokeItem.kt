package com.example.step12

data class JokeItem(
    val body: String,
    val id: Int
)
{
    fun toJokeItemDbEntity() = JokeItemDbEntity(
        body = body,
        id = id
    )
}