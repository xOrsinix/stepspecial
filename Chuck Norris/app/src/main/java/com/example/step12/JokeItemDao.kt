package com.example.step12

import androidx.room.*

@Dao
interface JokeItemDao {

    @Query("SELECT * FROM Current_jokes")
    fun getAll():List<JokeItemDbEntity>

    @Insert
    fun insert(joke:JokeItemDbEntity)

    @Query("DELETE from Current_jokes")
    fun deleteAll()
}