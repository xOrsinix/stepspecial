package com.example.step12

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentWebBinding

class WebFragment : Fragment() {

    lateinit var binding:FragmentWebBinding

    var URL = "https://www.icndb.com/api/"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWebBinding.inflate(inflater)
        requireActivity().title = "Api info"
        bNavSettings()
        webViewSetup(binding.wbView)

        val callback = object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                if (binding.wbView.canGoBack()){
                    binding.wbView.goBack()
                }
                else Navigation.findNavController(binding.root).navigate(R.id.fromWebToJokes)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)
        return binding.root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("LAST_URL",binding.wbView.url)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState!=null)
            URL = savedInstanceState.getString("LAST_URL", "https://www.icndb.com/api/")
        super.onCreate(savedInstanceState)
    }

    private fun bNavSettings(){
        binding.bNavWeb.selectedItemId = R.id.web_item
        binding.bNavWeb.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.web_item -> {}
                R.id.jokes_item -> {
                    Navigation.findNavController(binding.root).navigate(R.id.fromWebToJokes)}
            }
            true
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun webViewSetup(wb: WebView){
        wb.webViewClient= WebViewClient()
        wb.apply {
            loadUrl(URL)
            settings.safeBrowsingEnabled = true
        }

    }



}