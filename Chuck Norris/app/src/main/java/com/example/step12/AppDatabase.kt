package com.example.step12

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities =[
        JokeItemDbEntity::class
    ]
)

abstract class AppDatabase:RoomDatabase() {
    abstract fun getJokeItemDao():JokeItemDao
}