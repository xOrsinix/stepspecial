package com.example.step12

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.step12.databinding.FragmentJokesBinding
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.rxjava3.core.Observable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

const val BASE_URL = "https://my-json-server.typicode.com/DmitrySergeev02/ChuckNorrisJokes/"

class JokesFragment : Fragment() {

    lateinit var binding: FragmentJokesBinding

    lateinit var adapter: JokeItemAdapter

    lateinit var retrofitData:List<JokeItem>

    lateinit var jokeItemDao:JokeItemDao

    var SIZE:Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentJokesBinding.inflate(inflater)
        requireActivity().title = "Jokes"
        bNavSettings()
        rcViewSettings()
        fillAdapter()
        val db:AppDatabase by lazy<AppDatabase> {
            Room.databaseBuilder(requireContext(),AppDatabase::class.java,"database.db")
                .build()
        }
        jokeItemDao = db.getJokeItemDao()
        rcViewReInit()
        binding.btReload.setOnClickListener {
            reload()
        }
        return binding.root
    }

    private fun rcViewReInit(){
        CoroutineScope(Dispatchers.IO).launch {
            val jokeList = jokeItemDao.getAll()
            if (jokeList.isNotEmpty()){
                for (joke in jokeList)
                    adapter.addPostItem(joke.toJokeItem())
                binding.etCount.setText(jokeList.size.toString())
            }
        }
    }

    private fun reload(){
        try {
            SIZE = binding.etCount.text.toString().toInt()
            if (binding.etCount.text.toString().toInt() <= 30) {
                CoroutineScope(Dispatchers.IO).launch {
                    jokeItemDao.deleteAll()
                }
                adapter.clearAll()
                for (i in 0..SIZE - 1) {
                    CoroutineScope(Dispatchers.IO).launch {
                        Log.i("TAG", "${jokeItemDao.getAll()}")
                        jokeItemDao.insert(retrofitData[i].toJokeItemDbEntity())
                    }
                    adapter.addPostItem(retrofitData[i])
                }
            }
            else
            {
                Toast.makeText(context,"Please enter integer number between 0 and 30",Toast.LENGTH_SHORT).show()
            }
        }
        catch (e:Exception)
        {
            Toast.makeText(context,"Please enter integer number",Toast.LENGTH_SHORT).show()
        }
    }

    private fun rcViewSettings(){
        adapter = JokeItemAdapter()
        binding.rcView.layoutManager =LinearLayoutManager(requireContext())
        binding.rcView.adapter = adapter
    }

    private fun getData():Observable<JokeItem>{
        return Observable.create {
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            CoroutineScope(Dispatchers.IO).launch {
                retrofitData = retrofitBuilder.getData()
            }
            it.onComplete()
        }
    }

    private fun fillAdapter(){
        val dispose = getData()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{
              Log.i("TAG",it.localizedMessage)
            },{
            })
    }

    private fun bNavSettings(){
        binding.bNavJokes.selectedItemId = R.id.jokes_item
        binding.bNavJokes.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.jokes_item -> {}
                R.id.web_item -> {
                    Navigation.findNavController(binding.root).navigate(R.id.fromJokesToWeb)}
            }
            true
        }
    }

}