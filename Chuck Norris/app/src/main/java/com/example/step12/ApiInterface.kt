package com.example.step12

import retrofit2.http.GET

interface ApiInterface {
    @GET("jokes")
    suspend fun getData(): List<JokeItem>
}